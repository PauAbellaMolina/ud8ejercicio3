package dto;

public class Electrodomestico {

	//Atributos
	public double precioBase;
	public String color;
	public char consumoEnergetico;
	public double peso;
	
	final double PRECIOBASE = 100;
	final String COLOR = "blanco";
	final char CONSUMOENERGETICO = 'F';
	final double PESO = 5;

	public String toString() {
		return "Electrodomestico [precioBase=" + precioBase + ", color=" + color + ", consumoEnergetico="
				+ consumoEnergetico + ", peso=" + peso + "]";
	}

	//Constructor por defecto
	public Electrodomestico() {
		this.precioBase=PRECIOBASE;
		this.color=COLOR;
		this.consumoEnergetico=CONSUMOENERGETICO;
		this.peso=PESO;
	}
	
	//Constructor con el precio y peso. El resto por defecto
	public Electrodomestico(double precioBase, double peso) {
		this.precioBase=precioBase;
		this.color=COLOR;
		this.consumoEnergetico=CONSUMOENERGETICO;
		this.peso=peso;
	}
	
	//Constructor con todos los atributos
	public Electrodomestico(double precioBase, String color, char consumoEnergetico, double peso) {
		this.precioBase=precioBase;
		this.color=colorComprovacion(color);
		this.consumoEnergetico=consumoEnergeticoComprovacion(consumoEnergetico);
		this.peso=peso;
	}

	//Metodo para comprovar si el color introducido esta entre los aceptados
	private String colorComprovacion(String color) {
		if (color.equalsIgnoreCase("blanco") || color.equalsIgnoreCase("negro") || color.equalsIgnoreCase("rojo") || color.equalsIgnoreCase("azul") || color.equalsIgnoreCase("gris")) {
			return color;
		} else {
			return COLOR;
		}
	}

	//Metodo para comprovar si el caracter de consumo energetico esta entre los aceptados
	private char consumoEnergeticoComprovacion(char consumoEnergetico) {
		if (consumoEnergetico == 'A' || consumoEnergetico == 'B' || consumoEnergetico == 'C' || consumoEnergetico == 'D' || consumoEnergetico == 'C' || consumoEnergetico == 'F') {
			return consumoEnergetico;
		} else {
			return CONSUMOENERGETICO;
		}
	}
}
